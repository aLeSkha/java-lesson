import java.util.Scanner;

public class Animals {
    public static void main(String[] args) {

        System.out.println(" ");
        System.out.println("Nr. Name     Color      Weight      Age    numoflegs");
        System.out.println(" ");

           ObiectAnimals Animal1  = new ObiectAnimals (1, "bear",  "brown", "1.5m",  3, 4); 
           ObiectAnimals Animal2  = new ObiectAnimals (2, "bear",  "white", "1.4m",  4, 4);
           ObiectAnimals Animal3  = new ObiectAnimals (3, "dog",   "black", "0.5m",  1, 4);
           ObiectAnimals Animal4  = new ObiectAnimals (4, "cat",   "white", "0.3m",  2, 4);
           ObiectAnimals Animal5  = new ObiectAnimals (5, "cow",   "brown", "1.5m",  5, 4);
           ObiectAnimals Animal6  = new ObiectAnimals (6, "dunkey","black", "1.5m",  6, 4);
           ObiectAnimals Animal7  = new ObiectAnimals (7, "crow",  "black", "0.2m",  3, 2);
           ObiectAnimals Animal8  = new ObiectAnimals (8, "pig",   "pink",  "1m",    2, 4);        
           ObiectAnimals Animal9  = new ObiectAnimals (9, "wolf",  "black", "1m",    2, 4);
           ObiectAnimals Animal10 = new ObiectAnimals (10, "Fox",   "orange", "0.4m",1, 4);
    }
}    
class ObiectAnimals{
    String Name, color, weight; int age; int numoflegs; int Nr;
    
     public ObiectAnimals( int Nr, String Name, String color,  String weight, int age, int numoflegs){
     	this.Nr        = Nr;
        this.Name      = Name;
        this.color     = color;
        this.weight    = weight;
        this.age       = age;
        this.numoflegs = numoflegs;

 System.out.println( this.Nr + "    " + this.Name + "    " + this.color + "   " + this.weight + "     " + this.age + "     " + this.numoflegs);

    }
}
